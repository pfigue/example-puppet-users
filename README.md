## What is this?
A vagrant box to test the management of users in a Linux box via Puppet/Hiera

## Ingredients
+    Base box is *Ubuntu Server Precise 14.04 amd64 (source) Kernel is ready for Docker (Docker not included) Contains Chef, Puppet* (provider: VirtualBox).

+   Puppet3 (comes with the basebox)

+   librarian-puppet. Manually installed with `apt-get -qqy install ruby-dev make` and	`gem install librarian-puppet`

+   It uses the puppet module `mthibaut/users`.


## How to run it?

The idea is, after `vagrant up`, you `vagrant ssh` and then:

	sudo su -  # to become root

	# Hiera will use /vagrant/puppet/hiera as datadir
	cat > /etc/puppet/hiera.yaml <<EOF
	:backends: yaml
	:yaml:
	  :datadir: /vagrant/puppet/hiera
	:hierarchy: common
	:logger: console
	EOF

	apt-get -qqy install ruby-dev make  # librarian-puppet system dependencies
	gem install librarian-puppet  # librarian-puppet itself
	cd /vagrant/puppet/
	librarian-puppet install  # install the mthibaut/users module

	# Apply the example manifest:
	puppet apply --modulepath=/vagrant/puppet/modules/  /vagrant/puppet/site.pp


## How to test it?

Some suggestions after successfully running the manifest:

	grep john /etc/passwd
	grep john /etc/group
	grep jack /etc/passwd
	su - john  # and then `id`
	cat /home/john/.ssh/authorized_keys


## To Do

rspec/beaker/etc. Some Unit testing to test that nobody in *users_dev* belongs to group `sudo`, or that everybody in *users_sysadmin* does.
